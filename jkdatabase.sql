DROP TABLE IF EXISTS Page;
DROP TABLE IF EXISTS Person;
DROP TABLE IF EXISTS Comments;



.headers ON
.mode column
.explain ON


CREATE TABLE Comments(
id INTEGER PRIMARY KEY AUTOINCREMENT,
person_id INTEGER NOT NULL,
page_id INTEGER NOT NULL,
post VARCHAR(255),
Timestamp DATETIME DEFAULT CURRENT_TIMESTAMP,
FOREIGN KEY (page_id) REFERENCES Page(id),
FOREIGN KEY (person_id) REFERENCES Person (id)
);

CREATE TABLE Person(
id INTEGER PRIMARY KEY AUTOINCREMENT,
name VARCHAR(100) NOT NULL,
email VARCHAR (100) NOT NULL UNIQUE
);

CREATE TABLE Page(
    
id INTEGER PRIMARY KEY AUTOINCREMENT,
name VARCHAR(100) NOT NULL UNIQUE

);



INSERT INTO Person (id,name,email) VALUES (1,'Jordan', 'jordan@gmail.com');
INSERT INTO Person (id,name,email) VALUES (2,'Kerim', 'kerim@gmail.com');
INSERT INTO Person (id,name,email) VALUES (3,'Emre', 'emre@gmail.com');

INSERT INTO Page (name) VALUES ('/index.html');
INSERT INTO Page (name) VALUES ('/blog.html');

INSERT INTO Comments (person_id,page_id,post) VALUES (1,1,'This is a post by Jordan');
INSERT INTO Comments (person_id,page_id,post) VALUES (2,1,'This is a post by Kerim');
INSERT INTO Comments (person_id,page_id,post) VALUES (3,1,'This is a post by Emre');

 
