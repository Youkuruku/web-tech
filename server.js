// Run a node.js web server for local development of a static web site.
// Start with "node server.js" and put pages in a "public" sub-folder.
// Visit the site at the address printed on the console.

// The server is configured to be platform independent.  URLs are made lower
// case, so the server is case insensitive even on Linux, and paths containing
// upper case letters are banned so that the file system is treated as case
// sensitive even on Windows.

// Load the library modules, and define the global constants.
// See http://en.wikipedia.org/wiki/List_of_HTTP_status_codes.
// Start the server: change the port to the default 80, if there are no
// privilege issues and port number 80 isn't already in use.


var http = require("http");
var fs = require("fs");
var OK = 200, NotFound = 404, BadType = 415, Error = 500;
var types, banned;
var sql = require("sqlite3").verbose();
var db = new sql.Database('jor_ker_database.db');
var qs = require('querystring');
var jsdom = require('jsdom').verbose();

var person_id=0;
var page_id=0;


var person=[];
var post=[];



var another = require('./public/index.js');
start(8080);


function show (err,rows){
    if(err) throw err;
    console.log(rows);
    console.log("testler");
}

// Start the http service.  Accept only requests from localhost, for security.
function start(port) {
    types = defineTypes();
    banned = [];
    banUpperCase("./public/", "");
    console.log(banned);
    var service = http.createServer(handle);
    service.listen(port, "localhost");
    var address = "http://localhost";
    if (port != 80) address = address + ":" + port;
    console.log("Server running at", address);
}

//insert comments into related tables
function insertComment(name, email,cpage,text){


 var q = "INSERT INTO Person (name, email) VALUES ('" + name + "', '" + email + "');";
   db.run(q);


     db.each("SELECT id FROM Person where email='"+email+"'", function(err, rows) {

        person_id=rows.id;

           db.each("SELECT id FROM Page where name='"+cpage+"'", function(err, rows) {

         page_id=rows.id;

        insertComplete(person_id,page_id,text);

         });
  });

}

function insertComplete(person_id,page_id,text){

   var q2 = "INSERT INTO Comments (person_id,page_id, post) VALUES ('" + person_id + "', '" + page_id + "','" + text + "' );";
         db.run(q2);
}


function testFunction(response,cpage){


  //console.log(cpage);
    db.all("select name, email,post,timestamp from Person join Comments on Comments.person_id=Person.id", function(err, rows) {

         var text = JSON.stringify(rows);

   //   console.log(text);
        deliver(response, "text/plain", err, text);
 });

}
function anot(){


     person.forEach(function(hede){

    console.log(hede);
   })
}



function handle(request, response) {

    var url= request.url.toLowerCase();


    if (request.method == 'POST') {

        var currentPage="/index.html";
       // console.log(url);
       // currentPage=url;
      //  console.log("current page is coming");
      //  console.log(currentPage);

        var body = '';
        request.on('data', function (data) {
            body += data;

            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
                request.connection.destroy();
        });

        request.on('end', function () {
            var post = qs.parse(body);
            // use post['blah'], etc.
            //console.log("this is the post " + post.dname + "and "  + post.demail + " and " + post.textareaz);
            insertComment(post.dname, post.demail,currentPage,post.textareaz);

          var url = request.url.toLowerCase();
          if (url.endsWith("/")) url = url + "index.html";
          if (isBanned(url)) return fail(response, NotFound, "URL has been banned");
          var type = findType(url);
          if (type == null) return fail(response, BadType, "File type unsupported");
          var file = "./public" + url;
          fs.readFile(file, ready);

          function ready(err, content) { deliver(response, type, err, content); }
        });

    }

      else if (request.method=='GET') {

          if(request.url.match("anything"))
               {
            console.log(request.url);
             var currentPage="/index.html";
       // console.log(url);
       // currentPage=url;
            testFunction(response,currentPage);
                   //anot();
          }
          else {
          var url = request.url.toLowerCase();
          if (url.endsWith("/")) url = url + "index.html";
          if (isBanned(url)) return fail(response, NotFound, "URL has been banned");
          var type = findType(url);
          if (type == null) return fail(response, BadType, "File type unsupported");
          var file = "./public" + url;
          fs.readFile(file, ready);

          function ready(err, content) { deliver(response, type, err, content); }
        }
    }
}



// Find the content type to respond with, or undefined.
function findType(url) {
    var dot = url.lastIndexOf(".");
    var extension = url.substring(dot + 1);
    return types[extension];
}


// Deliver the file that has been read in to the browser.
function deliver(response, type, err, content) {
    if (err) return fail(response, NotFound, "File not found");
    var typeHeader = { "Content-Type": type };
    response.writeHead(OK, typeHeader);
    response.write(content);
    response.end();
}

// Give a minimal failure response to the browser
function fail(response, code, text) {
    var textTypeHeader = { "Content-Type": "text/plain" };
    response.writeHead(code, textTypeHeader);
    response.write(text, "utf8");
    response.end();
}

// Forbid any resources which shouldn't be delivered to the browser.
function isBanned(url) {
    for (var i=0; i<banned.length; i++) {
        var b = banned[i];
        if (url.startsWith(b)) return true;
    }
    return false;
}
// Check a folder for files/subfolders with non-lowercase names.  Add them to
// the banned list so they don't get delivered, making the site case sensitive,
// so that it can be moved from Windows to Linux, for example. Synchronous I/O
// is used because this function is only called during startup.  This avoids
// expensive file system operations during normal execution.  A file with a
// non-lowercase name added while the server is running will get delivered, but
// it will be detected and banned when the server is next restarted.
function banUpperCase(root, folder) {
    var folderBit = 1 << 14;
    var names = fs.readdirSync(root + folder);
    for (var i=0; i<names.length; i++) {
        var name = names[i];
        var file = folder + "/" + name;
        if (name != name.toLowerCase()) banned.push(file.toLowerCase());
        var mode = fs.statSync(root + file).mode;
        if ((mode & folderBit) == 0) continue;
        banUpperCase(root, file);
    }
}

// The most common standard file extensions are supported, and html is
// delivered as xhtml ("application/xhtml+xml").  Some common non-standard file
// extensions are explicitly excluded.  This table is defined using a function
// rather than just a global variable, because otherwise the table would have
// to appear before calling start().  NOTE: for a more complete list, install
// the mime module and adapt the list it provides.
function defineTypes() {
    var types = {
        html : "application/xhtml+xml",
        css  : "text/css",
        js   : "application/javascript",
        png  : "image/png",
        gif  : "image/gif",    // for images copied unchanged
        jpeg : "image/jpeg",   // for images copied unchanged
        jpg  : "image/jpeg",   // for images copied unchanged
        svg  : "image/svg+xml",
        json : "application/json",
        pdf  : "application/pdf",
        txt  : "text/plain",
        ttf  : "application/x-font-ttf",
        woff : "application/font-woff",
        aac  : "audio/aac",
        mp3  : "audio/mpeg",
        mp4  : "video/mp4",
        webm : "video/webm",
        ico  : "image/x-icon", // just for favicon.ico
        xhtml: undefined,      // non-standard, use .html
        htm  : undefined,      // non-standard, use .html
        rar  : undefined,      // non-standard, platform dependent, use .zip
        doc  : undefined,      // non-standard, platform dependent, use .pdf
        docx : undefined,      // non-standard, platform dependent, use .pdf
    }
    return types;
}


// Serve a request by delivering a file.
